import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from 'nestjs-typegoose';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  const exec = { exec: jest.fn() };
  const authRepositoryFactory = () => ({
    find: () => exec,
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          useFactory: authRepositoryFactory,
          provide: getModelToken('UserModel'),
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
